import datetime
import json
from flask import abort, jsonify,request
import requests
from flask_restful import Resource
from sqlalchemy import except_
from trainers_api.models import(Trainer,
 trainer_schema, trainers_schema, PokemonOwnedTrainer,pokemons_owned_trainer_schema, pokemon_owned_trainer_schema)
from trainers_api.ext.database import db
import jwt


def _lista_json_pokemons(lista_pokemons_trainer):
                    lista_pokemons = []
                    for pokemon_trainer_owned in lista_pokemons_trainer:
                        resource_path =API+'{}'.format(pokemon_trainer_owned.pokemon_id)
                        response_pokemon_api = requests.get(resource_path)
                        lista_pokemons.append(response_pokemon_api)
                    
                    return [pokemon.json() for pokemon in lista_pokemons]

API = 'https://pokeapi.co/api/v2/pokemon/'
SECRET_KEY = 'jadkfbsdkjbfbh'

class TrainerAuthenticationResource(Resource):
    def post(self):
        email =  request.json['email']
        password =request.json['password']

        trainer = Trainer.query.filter_by(email=email).first() 
        if trainer is None:
             return  abort(
                 code =  404,
                 description ="Trainer not found"
                 )
        if trainer.password != password:
            abort(403,description="suas credenciais estão incorretas!")
        
        validity = datetime.datetime.utcnow() + datetime.timedelta(minutes=5)
        payload ={
            "id": trainer.id,
            "exp": validity
        }

        token = jwt.encode(payload,SECRET_KEY)
        return jsonify({"id": trainer.id,"token": token})


class ListaTrainerResource(Resource):

    def get(self):
        try:
            trainers = Trainer.query.all()
            if len(trainers) ==0 :
                return jsonify({[]})
            return jsonify(trainers_schema.dump(trainers))
        except ValueError as err:
            abort(code=500,description="Internal server error")


class TrainerByIdResource(Resource):
    def get(self, trainer_id):
        try:
            if isinstance(int(trainer_id), int):
                trainer = Trainer.query.filter_by(id=trainer_id).first() or abort(404,description="Trainer not found") 
                if trainer is None:
                    abort(
                    code =  404,
                    description ="Trainer not found"
                 )
                return jsonify(trainer_schema.dump(trainer))
        except ValueError as ex:
            abort(500,description="Internal server error")

class CreateTrainerResource(Resource):    
    def post(self):
            nickname =request.json['nickname']
            first_name=request.json['first_name']
            last_name =request.json['last_name']
            email =  request.json['email']
            password =request.json['password']
            team = request.json['team']
            try:
                trainer = Trainer.query.filter_by(nickname=nickname,first_name= first_name, last_name=last_name, email= email, password=password, team=team).first()
                if trainer:
                    abort(code= 400, description=  "Trainer já está cadastrado")
                
                trainer = Trainer(nickname, first_name, last_name,email, password, team)
                db.session.add(trainer)
                db.session.commit()
                return jsonify(trainer_schema.dump(trainer))
            except ValueError as ex:
                 abort(500,description="Internal server error")
              


class PokemonOwnedTrainerResource(Resource):
    def post(self, trainer_id):
            trainer = Trainer.query.filter_by(id=trainer_id).first() 
            try:

                if trainer is None:
                    abort(
                    code =  404,
                    description ="Trainer not found"
                 )

                token_jwt =  request.headers.get('token')
                trainer_id_decoded =jwt.decode(token_jwt, SECRET_KEY,algorithms=['HS256'])

                if trainer.id != trainer_id_decoded.get('id'):
                    abort(code = 403,
                    description =  "you don't have permission to perform this action. Reason: trainer id are mismatched."
                    )
                
                #TODO: extrair para um metodo get_date_now_timestamp() e colocar em um arquivo utilitário
                date_and_hour_now = datetime.datetime.utcnow() - datetime.timedelta(hours=+3)
                date_and_hour_now = date_and_hour_now.timestamp()

                if trainer_id_decoded.get('exp') < date_and_hour_now :
                    abort(code = 401,
                    message =  "You are not allowed to access this route without a token.")
                    
                name =request.json['name']
                level=request.json['level']
                pokemon_id =request.json['pokemon_id']
                pokemon_owned_trainer = PokemonOwnedTrainer(name, level,pokemon_id, trainer.id)
                db.session.add(pokemon_owned_trainer)
                db.session.commit()    

                api_request =API+'{}'.format(pokemon_id)
                response_pokemon_api = requests.get(api_request)

                return jsonify({"id": pokemon_owned_trainer.id, "name":name, "level": level,"pokemon_data":response_pokemon_api.json()})
            except jwt.ExpiredSignatureError as err:
                try:
                  return abort(401,description= "you cannot access this route because your token has expired or is invalid.")
                except Exception:
                    return abort(500,description= "Internal server Error.")

class ListaPokemonsOwnedByTrainerId(Resource):    
    def get(self, trainer_id):
            lista_pokemons_trainer = PokemonOwnedTrainer.query.filter_by(trainer_id=trainer_id).all()    
            try:

                if len(lista_pokemons_trainer) ==0 :
                    abort(
                    code= 404,
                    description= "trainer or pokemon not found!"
                    )

                return jsonify(pokemons_owned_trainer_schema.dump(lista_pokemons_trainer),{"pokemon_data": _lista_json_pokemons(lista_pokemons_trainer)})
            except jwt.ExpiredSignatureError as err:
                try:
                  return abort(401,description= "you cannot access this route because your token has expired or is invalid.")
                except Exception:
                    return abort(500,description= "Internal server Error.")


class ListaPokemonOwnedTrainerResource(Resource):    
    def get(self, trainer_id, pokemon_id):
            trainers = PokemonOwnedTrainer.query.filter_by(id=trainer_id, pokemon_id=pokemon_id).all()    
            try:

                if len(trainers) ==0 :
                    abort(404,description= "trainer or pokemon not found!")

                api_request =API+'{}'.format(pokemon_id)
                response_pokemon_api = requests.get(api_request)

                return jsonify(pokemons_owned_trainer_schema.dump(trainers),{"pokemon_data":response_pokemon_api.json()})
            except jwt.ExpiredSignatureError as err:
                try:
                  return abort(401,description= "you cannot access this route because your token has expired or is invalid.")
                except Exception:
                    return abort(500,description= "Internal server Error.")

class DeletePokemonOwnedTrainerResource(Resource):
    def delete(self, trainer_id,pokemon_id):
            pokemon_owned_trainer = PokemonOwnedTrainer.query.filter_by(trainer_id=trainer_id,pokemon_id=pokemon_id).first() 
            try:

                if pokemon_owned_trainer is None:
                    abort(
                    code= 404,
                    description="trainer or pokemon not found!"
                    )

                token_jwt =  request.headers.get('token')
                trainer_id_decoded =jwt.decode(token_jwt, SECRET_KEY,algorithms=['HS256'])

                if pokemon_owned_trainer.trainer_id != trainer_id_decoded.get('id'):
                    abort(
                    code=403,
                    description= "you don't have permission to perform this action. Reason: trainer id are mismatched."
                    )
                
                #TODO: extrar para um metodo get_date_now_timestamp()
                date_and_hour_now = datetime.datetime.utcnow() - datetime.timedelta(hours=+3)
                date_and_hour_now = date_and_hour_now.timestamp()

                if trainer_id_decoded.get('exp') < date_and_hour_now :
                    abort(
                    code = 401,
                    description =  "You are not allowed to access this route without a token."
                    )
                
                db.session.delete(pokemon_owned_trainer)
                db.session.commit()    

                api_request =API+'{}'.format(pokemon_id)
                response_pokemon_api = requests.get(api_request)

                return jsonify(pokemon_owned_trainer_schema.dump(pokemon_owned_trainer),{"pokemon_data":response_pokemon_api.json()})
            except jwt.ExpiredSignatureError as err:
                try:
                  return abort(401,description= "you cannot access this route because your token has expired or is invalid.")
                except Exception:
                    return abort(500,description= "Internal server Error.")
