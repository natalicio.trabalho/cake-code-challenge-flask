from crypt import methods
from flask import Blueprint
from flask_restful import Api

from .resources import DeletePokemonOwnedTrainerResource,ListaPokemonsOwnedByTrainerId, PokemonOwnedTrainerResource, TrainerAuthenticationResource,CreateTrainerResource, TrainerByIdResource, ListaTrainerResource, PokemonOwnedTrainerResource,ListaPokemonOwnedTrainerResource

bp = Blueprint("restapi", __name__, url_prefix="/api/v2")
api = Api(bp)


def init_app(app):
    api.add_resource(TrainerAuthenticationResource, "/trainer/authenticate", methods=["POST"])

    api.add_resource(CreateTrainerResource, "/trainer", methods=["POST"])
    api.add_resource(ListaTrainerResource, "/trainer", methods=["GET"])
    api.add_resource(TrainerByIdResource, "/trainer/<trainer_id>",methods=["GET"])
    api.add_resource(ListaPokemonsOwnedByTrainerId, "/trainer/<trainer_id>/pokemon",methods=["GET"])
    api.add_resource(PokemonOwnedTrainerResource, "/trainer/<trainer_id>/pokemon", methods=["POST"])
    api.add_resource(ListaPokemonOwnedTrainerResource, "/trainer/<trainer_id>/pokemon/<pokemon_id>", methods=["GET"])
    api.add_resource(DeletePokemonOwnedTrainerResource, "/trainer/<trainer_id>/pokemon/<pokemon_id>", methods=["DELETE"])

    app.register_blueprint(bp)
