from flask import Blueprint

from .views import index, get_trainer_by_id

bp = Blueprint("webui", __name__, template_folder="templates")

bp.add_url_rule("/", view_func=index)
bp.add_url_rule(
    "/trainer/<trainer_id>", view_func=get_trainer_by_id, endpoint="trainerview"
)


def init_app(app):
    app.register_blueprint(bp)
