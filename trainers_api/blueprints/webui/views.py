from flask import abort, render_template
from trainers_api.models import Trainer


def index():
    trainers = Trainer.query.all()
    print(len(trainers))
    return render_template("index.html", trainers=trainers)


def get_trainer_by_id(trainer_id):
    trainer = Trainer.query.filter_by(id=trainer_id).first() or abort(
        404, "Trainer não encontrado"
    )
    return render_template("trainer.html", trainer=trainer)
