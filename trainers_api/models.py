from sqlalchemy import BigInteger, ForeignKey
from trainers_api.ext.database import db
from sqlalchemy_serializer import SerializerMixin, Serializer
from trainers_api.ext.serialization import ma



class Trainer(db.Model, SerializerMixin,Serializer):
    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(200))
    first_name = db.Column(db.String(200))
    last_name = db.Column(db.String(200))
    email =  db.Column(db.String(200))
    password =  db.Column(db.String(200))
    team = db.Column(db.String(200))
    pokemons_owned =  db.Column(db.BigInteger)
    # pokemons_owned_trainer = db.relationship('PokemonOwnedTrainer', backref='trainer', lazy='select')

    def __init__(self, nickname, first_name, last_name, email, password, team, pokemons_owned= 3):
        self.nickname = nickname
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.password = password
        self.team = team
        self.pokemons_owned = pokemons_owned

class TrainerSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ("id", "nickname", "first_name","last_name","email","team","pokemons_owned")



trainer_schema = TrainerSchema()
trainers_schema = TrainerSchema(many=True)


class PokemonOwnedTrainer(db.Model, SerializerMixin,Serializer):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    level =  db.Column(db.BigInteger)
    pokemon_id =  db.Column(db.BigInteger)
    trainer_id = db.Column(db.Integer, db.ForeignKey('trainer.id'), nullable= False)
    trainer = db.relationship("Trainer", backref="trainer", uselist=False)
    

    def __init__(self, name, level, pokemon_id, trainer_id):
        self.name = name
        self.level = level
        self.pokemon_id = pokemon_id
        self.trainer_id = trainer_id

class PokemonOwnedTrainerSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ("name", "level","podemon_id","trainer_id")


pokemon_owned_trainer_schema =PokemonOwnedTrainerSchema()
pokemons_owned_trainer_schema = PokemonOwnedTrainerSchema(many=True)


class TrainerAuthentication(db.Model, SerializerMixin):
    id = db.Column(db.BigInteger, primary_key=True)
    email = db.Column(db.String(500))
    password = db.Column(db.String(512))

    def __init__(self,id:int, email:str, password:str):
        self.id = id
        self.email = email
        self.password = password

class TrainerAuthenticationSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ("id", "nickname", "first_name","last_name","email","team","pokemons_owned")



trainer_authentication_schema = TrainerAuthenticationSchema()
trainers_authentication_schema = TrainerAuthenticationSchema(many=True)
        
