from flask_admin import Admin
from flask_admin.base import AdminIndexView
from flask_admin.contrib import sqla
from flask_simplelogin import login_required
from werkzeug.security import generate_password_hash

from trainers_api.ext.database import db
from trainers_api.models import TrainerAuthentication, Trainer,PokemonOwnedTrainer

# Proteger o admin com login via Monkey Patch
AdminIndexView._handle_view = login_required(AdminIndexView._handle_view)
sqla.ModelView._handle_view = login_required(sqla.ModelView._handle_view)
admin = Admin()


class UserAdmin(sqla.ModelView):
    column_list = ['email']
    can_edit = False



def init_app(app):
    admin.name = app.config.TITLE
    admin.template_mode = "bootstrap3"
    admin.init_app(app)
    admin.add_view(sqla.ModelView(Trainer, db.session))
    admin.add_view(sqla.ModelView(PokemonOwnedTrainer, db.session))
    admin.add_view(UserAdmin(TrainerAuthentication, db.session))
