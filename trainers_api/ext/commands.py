import click
from trainers_api.ext.database import db
from trainers_api.ext.auth import create_user
from trainers_api.models import Trainer, TrainerAuthentication


def create_db():
    """Creates database"""
    db.create_all()


def drop_db():
    """Cleans database"""
    db.drop_all()


def populate_db():
    data = [
        Trainer(nickname="natlaicio",first_name = "natalicio", last_name="nascimento",
        email="natalicio@gmail.com", password= "123", team= "teste", pokemons_owned=3),
        TrainerAuthentication(1,'admin',1234)
    ]
    db.session.bulk_save_objects(data)
    db.session.commit()
    return (Trainer.query.all(), TrainerAuthentication.query.all())


def init_app(app):
    # add multiple commands in a bulk
    for command in [create_db, drop_db, populate_db]:
        app.cli.add_command(app.cli.command()(command))

    # add a single command
    @app.cli.command()
    @click.option('--username', '-u')
    @click.option('--password', '-p')
    def add_user(username, password):
        return create_user(username, password)
