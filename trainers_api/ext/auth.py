from flask_simplelogin import SimpleLogin
from werkzeug.security import check_password_hash, generate_password_hash
from trainers_api.ext.database import db
from trainers_api.models import TrainerAuthentication


def verify_login(user):
    """Valida o usuario e senha para efetuar o login"""
    email = user.get('username')
    password = user.get('password')
    if not email or not password:
        return False
    existing_user = TrainerAuthentication.query.filter_by(email=email).first()
    if not existing_user:
        return False
    if existing_user.password == password:
        return True
    return False


def create_user(email, password):
    if TrainerAuthentication.query.filter_by(email=email).first():
        raise RuntimeError(f'{email} já esta cadastrado')
    user = TrainerAuthentication(email=email, password=generate_password_hash(password))
    db.session.add(user)
    db.session.commit()
    return user


def init_app(app):
    SimpleLogin(app, login_checker=verify_login)
