# Cake Code Challenge-Resource Trainer

___
Decisões Arquiteturais do Projeto
---
Escolhi o flask para esse projeto pelo nivel de abstração que ele proporciona permitindo facilmente trocar alguma extensão. E pela fato de ser um microframework apresenta um alto nivel de desempenho.
Seguí um modelo arquitetural voltado para a camada de negócio. Apesar da aplicação utilizar alguns plugins de terceiros(que ficam no modulo ext), no geral o projeto em si é agnostico em relação a uma extensão específica.

Utilizei o padrão aplication factory que encapsula o app em um método faz alguma alteração e o retorna modifcado, para não existir a possibilidade de importar o app diretamente e acabar gerando o famoso circular import.



## Clone

```bash
git clone https://gitlab.com/natalicio.trabalho/cake-code-challenge-flask.git
```



## Ambiente

Python 3.9+
Ative a sua virtualenv

```bash
pip install -r requirements.txt
pip install -r requirements_dev.txt
pip install -r requirements_test.txt
```
ou  Simplesmente um :
```bash
 make install
```
## Testando

```bash
pytest trainers_api/tests -v
```

## Executando

```bash

flask create-db  # rodar uma vez
flask populate-db # rodar uma vez
flask add-user -u admin -p 1234  # adiciona usuario admin
FLASK_DEBUG=1 FLASK_APP=trainers_api/app.py flask run
ou 
flask run

```


Acesse:

- Admin: http://localhost:5000/admin/
  - user: admin, senha: 1234
- API GET:
  - http://localhost:5000/api/v2/trainer
  -http://localhost:5000/api/v2/trainer/1
  - http://localhost:5000/api/v2/trainer/1/pokemon

- API POST:
  - Os recursos que precisam passsar o token de ser adicionado no header segue esse modelo:

```bash
token = eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.
eyJlbWFpbCI6ImF5bGFuQGJvc2Nhcmluby5
jb20iLCJwYXNzd29yZCI6InlhMGdzcWh5NHd6dnV2YjQifQ.yN_8-
Mge9mFgsnYHnPEh_ZzNP7YKvSbQ3Alug9HMCsM

```
## Estrutura do Projeto

```bash
.
├── Makefile
├── trainers_api  (MAIN PACKAGE)
│   ├── app.py  (APP FACTORIES)
│   ├── blueprints  (BLUEPRINT FACTORIES)
│   │   ├── __init__.py
│   │   ├── restapi  (REST API)
│   │   │   ├── __init__.py
│   │   │   └── resources.py
│   │   └── webui  (FRONT END)
│   │       ├── __init__.py
│   │       ├── templates
│   │       │   ├── index.html
│   │       │   └── trainer.html
│   │       └── views.py
│   ├── ext (EXTENSION FACTORIES)
│   │   ├── admin.py
│   │   ├── appearance.py
│   │   ├── auth.py
|   |   |__authentication.py
|   |   |__
│   │   ├── commands.py
│   │   ├── configuration.py
│   │   ├── database.py
│   │   └── __init__.py
│   ├── __init__.py
│   ├── models.py  (DATABASE MODELS)
│   └── tests  (TESTS)
│       ├── conftest.py
│       ├── __init__.py
│       └── test_api.py
├── README.md
├── requirements_dev.txt
├── requirements_test.txt
├── requirements.txt
└── settings.toml  (SETTINGS)

7 directories, 26 files
```

## Painel Admin
- Painel com o objetivo de fazer o cadastro de trainers e demais entidades.

![Preview](https://gitlab.com/natalicio.trabalho/imagens-cake-erp-trainer/-/blob/main/home.png)


![Preview](https://gitlab.com/natalicio.trabalho/imagens-cake-erp-trainer/-/blob/main/admin.png)

![Preview](https://gitlab.com/natalicio.trabalho/imagens-cake-erp-trainer/-/blob/main/cadastro-traineer.png)

![Preview](https://gitlab.com/natalicio.trabalho/imagens-cake-erp-trainer/-/blob/main/trainer_owned_pokemon.png)
